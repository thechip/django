from django.shortcuts import render
from students.forms import studentForm

def formexample(request):

    form=studentForm()
   # print(request.POST) (it gives the info of student in cmd dictionary form)
    if request.method =='POST':

        form = studentForm(request.POST)
        if form.is_valid():
            pass
            #all form input are valid,can save data in db

    return render(request,'form_example.html',{'form':form})


def studentinfo(request):
    info={
        'name':'kinjal',
        'email':'kinjusorathiya@gmail.com',
        'studentslist':['a','b','c'],
        'Address':{'Location':'BTM','Pincode':560059}
    }
    return render(request,'hey.html',info)
def studentsinfo(request):
    return render(request,'studentinfo.html')


# Create your views here.
