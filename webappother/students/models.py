from django.db import models


class StudentModel(models.Model):
    Name=models.CharField(max_length=100, blank=True, null=True, default=None)
    email=models.EmailField(unique=True)
    address= models.CharField(max_length=250, blank=True, null=True, default=None)
    city = models.CharField(max_length=100, blank=True, null=True, default=None)


    def __str__(self):                                                  #for UI modify the object name
        return self.name

    class Meta:
        db_table='student_profile'



class StudentEducation(models.Model):
    edu_name=models.CharField(max_length=100,primary_key=True,auto_created=True)
    edu_grade=models.CharField(max_length=50)


    def __str__(self):                                                  #for UI modify the object name
        return self.name
    class Meta:
        db_table='student_education'


# Create your models here.
