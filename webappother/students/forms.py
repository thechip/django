from django import forms


class studentForm(forms.Form):

    cn=(
        ('','--Select Option--'),
        ('bng','Bangalore'),
        ('pune','Pune'),
        ('Hyd','Hydrabad')
    )
    Cite_Name=forms.ChoiceField(choices=cn)

    OccupationData=(
        ('student','student'),
        ('Self Employee','self employee'),
        ('Employee','employee')
    )

    Occupation=forms.ChoiceField(choices=OccupationData,widget=forms.RadioSelect)

    student_name=forms.CharField(   #default validation
        required=True,
        min_length=8,
        max_length=20,
        label='Name',
        help_text="please enter atleast 8 characters",
        error_messages={
            'required':'student name cannot blank',
            'min_length':'new message'
        }
    )
    student_email = forms.EmailField()
    student_address=forms.CharField(widget=forms.Textarea)
    Password1 = forms.CharField(label='Password', widget=forms.PasswordInput)
    Password2 = forms.CharField(label='confirm_password', widget=forms.PasswordInput)
    Is_active=forms.BooleanField()  #for required field
    Is_active2=forms.CharField(widget=forms.CheckboxInput)   #for not required field


#custom validation
    def clean(self):
        form_data = self.cleaned_data
        if 'student_name' in form_data and form_data['student_name'].isdigit():
            self.errors['student_name']=['Invalid student name!']
        if 'student_email' in form_data and form_data['student_email'].find('@myTectra.com')<0:
            self.errors['student_email']=['Email not allowed!']



        return form_data
