from django.urls import path
from testapp import views
urlpatterns=[
    path('hi-django',views.action),
    path('jango',views.helloDjango),
    path('static',views.static_example)
]