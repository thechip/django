from django.urls import path
from Restapi.views import EmployeeDetails,EmployeeList


urlpatterns=[
    path("employee",EmployeeDetails.as_view()),              #class based view
    path("employee/<int:id>",EmployeeList.as_view())
]