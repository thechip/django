from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
#from Restapi.serializers.Employee import EmployeeSerializers
#from utils.EmployeeUtil import Employee

from Restapi.EmployeeSerializer import EmployeeSerializer

class EmployeeDetails(APIView):

    def post(self,request):

        empSeri = EmployeeSerializer(data=request.data)
        if empSeri.is_valid():
            return Response({"data":empSeri.data},status.HTTP_201_CREATED)

        return Response (empSeri.errors,status.HTTP_400_BAD_REQUEST)


    def get(self,request):

        return Response({"success":True},status.HTTP_200_OK)                 #200:SUCCESS


class EmployeeList(APIView):

    def delete(self, request,id):
        return Response({"message":"Item Deleted"},status.HTTP_200_OK)


    def put(self,request,id):
        return Response({"message":"Ited Update"},status.HTTP_200_OK)



# Create your views here.
