from django.db import models


class Employeemodel(models.Model):
    emp_name=models.CharField(max_length=100 ,blank=True, null=True, default=None)
    emp_email=models.EmailField(unique=True)
    emp_address=models.CharField(max_length=250)

    class Meta:
        db_table='employee'

# Create your models here.
