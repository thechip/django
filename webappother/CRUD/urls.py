from django.urls import path
from CRUD import views

urlpatterns=[
    path('create',views.create),
    path('index',views.index),
    path('update', views.update),
    path('delete',views.delete),
    path('view', views.view)
]