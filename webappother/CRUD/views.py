from django.shortcuts import render,redirect,HttpResponse
from CRUD.forms import EmployeeForm
from CRUD.models import Employeemodel
from django.contrib.auth.decorators import login_required

@login_required(login_url='/login')                                              #genrator
def create(request):
    empForm=EmployeeForm()
    if request.method =='POST':
        empForm = EmployeeForm(request.POST)
        if empForm.is_valid():
            empModel=Employeemodel()
            empModel.emp_name=empForm.cleaned_data['emp_name']
            empModel.emp_email=empForm.cleaned_data['emp_email']
            empModel.emp_address=empForm.cleaned_data['emp_address']
            empModel.save()
            return redirect(index)
    return render(request,'crud/create.html',{'empForm':empForm})



@login_required(login_url='/login')
def index(request):
    #select * from employee
    resultset=Employeemodel.objects.all()
    return render(request,'crud/index.html',{'data':resultset})


def update(request):

    try:
        id = request.GET['id']
    except:
        return HttpResponse('<h3>Somthing Went Wrong!</h3')
    result=Employeemodel.objects.get(id=id)
    empForm = EmployeeForm(instance=result)
    if request.method == 'POST':
        empForm = EmployeeForm(request.POST,instance=result)
        if empForm.is_valid():
            empModel = Employeemodel()
            empModel.id=id
            empModel.emp_name = empForm.cleaned_data['emp_name']
            empModel.emp_email = empForm.cleaned_data['emp_email']
            empModel.emp_address = empForm.cleaned_data['emp_address']
            empModel.save()
            return redirect(index)
    return render(request, 'crud/update.html', {'empForm': empForm})


def delete(request):
    id=request.GET['id']
    result=Employeemodel.objects.get(id=id)
    result.delete()
    return redirect(index)
def view(request):
    id=request.GET['id']
    result=Employeemodel.objects.get(id=id)
    return render(request,'CRUD/view.html',{'info':result})



# Create your views here.
