from django import forms
from CRUD.models import Employeemodel


#forms.Form, forms,ModelForm
class EmployeeForm(forms.ModelForm):
    emp_name=forms.CharField(min_length=8)
    class  Meta:

        model=Employeemodel
        fields='__all__'
        #or
        #fields=('emp name','emp email','emp address')
