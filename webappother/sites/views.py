from django.shortcuts import render,redirect
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from django.contrib.auth import login as UserLogin,logout as UserLogout,authenticate
from sites.forms import LoginForm
from sites.forms import RegistrationForm




def registration(request):
    if request.user.username:
        return redirect(dashBoard)
    message = ""
    registarationForm=RegistrationForm()
    if request.method == 'POST':
        registarationForm=RegistrationForm(request.POST)
        if registarationForm.is_valid():
            formData=registarationForm.cleaned_data
            user=User()
            user.username=formData['username']
            user.email=formData['email']
            user.set_password(formData['password1'])  #password hasing done here
            user.save()
            message='Registration done sucessfully,Thank You'


    return render(request,'auth/registration.html',{'form':registarationForm,'message':message})


def login(request):
    if request.user.username:
        return redirect(dashBoard)
    isValidUser=" "
    form=LoginForm()
    if request.method == "POST":
        form = LoginForm(request.POST)
        if form.is_valid():
            email = form.cleaned_data['email']
            password = form.cleaned_data['password']
            user = authenticate(email=email, password=password)
            if user is None:
                isValidUser = "Invalid login detalis"
            else:
                UserLogin(request,user)                                   #user object
                return redirect(dashBoard)
    return render(request,'auth/login.html',{'form':form,'isvalidUser':isValidUser})



def dashBoard(request):
    return render(request,'auth/dashboard.html')


def logout(request):
    UserLogout(request)
    return redirect(login)



# Create your views here.
