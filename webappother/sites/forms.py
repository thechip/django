from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User



class RegistrationForm(UserCreationForm):
    email=forms.EmailField(widget=forms.TextInput(attrs={'placeholder': 'Enter Email Address'}))
    class Meta:
        model=User
        fields=('username','email','password1','password2')


class LoginForm(forms.Form):
    email=forms.EmailField(widget=forms.TextInput)
    password=forms.CharField(widget=forms.PasswordInput)