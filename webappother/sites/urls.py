from django.urls import path
from sites import views
urlpatterns=[
    path('registration',views.registration),
    path('login',views.login),
    path('dashboard',views.dashBoard),
    path('logout',views.logout)
]